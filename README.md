# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

### function init () :
1. 在程式一開始執行，將所有變數初始化
2. 創建空白畫布

### function ev_canvas (ev):
1. 滑鼠進行mousedown, mouseup, mousemove時呼叫
2. 創建空白畫布

### 輸入字串 document.addEventListener('keydown',function(ev)
1. 接受鍵盤輸入，將使用者輸入的字串顯示在滑鼠點擊的地方
2. 使用ev.key判斷書是否按下換行或刪除，並且進行相對應的行為
3. 文字的大小依照畫筆大小變動
4. 字型的更動可點擊F1（arial）、F2(Georgia)改變
5. 打字請點選右上方的A

###cursor
1. 使用css控制，在鼠標移動到可點選的element上方時由十字變成指標

### color
1. 點擊上方不同色塊以切換畫筆、形狀、文字的顏色
### 大小
1. 點擊上方數字以切換畫筆、形狀、文字的粗細及大小

### function img_update ()
1. 在原始的canvas上創建一層tempcanvas，在每一次畫布變動時更新
2. 讓繪製圖型的時候不會產生殘影

### 畫筆 tools.chalk = function ()
1. 初始狀態，提供使用者畫筆的功能
2. 為鉛筆圖像
### 長方形 tools.rectangle = function ()
1. 提供使用者繪製長方形的功能
2. 為橘色長方形圖像
### 圓形 tools.circle= function ()
1. 提供使用者繪製圓形的功能
2. 為紅色圓形圖像
### 線條 tools.line= function ()
1. 提供使用者繪製線條的功能
2. 為黑色斜線圖像
### 橡皮擦 tools.eraser = function ()
1. 橡皮擦功能（粗細和畫筆相同，但是顏色固定為底色）
2. 為粉紅色長方形的橡皮擦圖像

### 重置 reset
1. 將畫布清空
2. 為循環的圖像

### 上一步 cUndo
1. undo功能，為下一步的圖像
2. 將上一步的圖片重新繪製

### 下一步 cRedo
1. redo功能，為返回的圖像
2. 將下一步的圖片重新繪製

### cPush
1. 在每一次畫面變動時將當前圖片push進stack中

### 儲存圖片 document.getElementById("save")
1. 讓使用者存取當前圖片，格式為png
2. 點擊畫布下方的上傳圖片

### previewImage = function(input)
1. 上傳圖片並且在畫布之下方預覽

### setImage = function()
1. 使用者在畫布下方輸入欲將圖片放置的座標及大小
2. 點擊setimage將圖片放置在畫布之上

### rainbow 
1. 讓使用者創造彩虹色的畫筆、圖形、文字
2. 點擊彩虹圖像



參考網址：
https://code.tutsplus.com/tutorials/how-to-create-a-web-based-drawing-application-using-canvas--net-14288?_ga=2.39372901.718252582.1554271019-1464874992.1553771808
