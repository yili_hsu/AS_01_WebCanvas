    var context;
    var canvas, canvaso, contexto; 
    var type = ['chalk','rect','circle','line','eraser'];
    var colors = ['red', 'blue', 'green', 'purple', 'yellow', 'orange', 'pink', 'black', 'white', 'ebebeb', 'rainbow'];
    var rainlicked = false;
    let hue = 0;
    var lastX,lastY;
    var laststrokeStyle = "#FFFFFF";
    // Default tool. (chalk, line, rectangle) 
    var tool; 
    var tool_default = 'chalk'; 
    // Check for the canvas tag onload.
    var cPushArray = new Array();
    var cStep = -1;
    var mouseX=0, mouseY=0, startingX=0;
    var inText = false;
    var fonttype = "Georgia";
    function cPush() {
        cStep++;
        if (cStep < cPushArray.length) { cPushArray.length = cStep; }

        cPushArray.push(document.getElementById('drawingCanvas').toDataURL());
    }

    function cRedo() {
        if (cStep < cPushArray.length-1) {
            cStep++;
            var canvasPic = new Image();
            canvasPic.src = cPushArray[cStep];
            canvasPic.onload = function () { context.drawImage(canvasPic, 0, 0); }
            document.title = cStep + ":" + cPushArray.length;
        }
    }

    function cUndo() {
        if (cStep > 0) {
            cStep--;
            var canvasPic = new Image();
            canvasPic.src = cPushArray[cStep];
            canvasPic.onload = function () { context.drawImage(canvasPic, 0, 0); }
            document.title = cStep + ":" + cPushArray.length;
        }else if(cStep == 0){
            console.log(cStep)
            var canvasPic = new Image();
            canvasPic.src = cPushArray[cStep];
            canvasPic.onload = function () { context.drawImage(canvasPic, 0, 0); }
            document.title = cStep + ":" + cPushArray.length;
        }
    }
    if(window.addEventListener) { 
    window.addEventListener('load', function () {
    function init () { 
    canvaso = document.getElementById('drawingCanvas'); 
    if (!canvaso) { 
    alert('Error! The canvas element was not found!'); 
    return; 
    } 
    if (!canvaso.getContext) { 
    alert('Error! No canvas.getContext!'); 
    return; 
    } 
    // Create 2d canvas. 
    contexto = canvaso.getContext('2d'); 
    if (!contexto) { 
    alert('Error! Failed to getContext!'); 
    return; 
    } 
    // Build the temporary canvas. 
    var container = canvaso.parentNode; 
    canvas = document.createElement('canvas'); 
    if (!canvas) { 
    alert('Error! Cannot create a new canvas element!'); 
    return; 
    } 
    canvas.id     = 'tempCanvas'; 
    canvas.width  = canvaso.width; 
    canvas.height = canvaso.height; 
    container.appendChild(canvas); 
    context = canvas.getContext('2d'); 
    context.strokeStyle = "#FFFFFF";// Default line color. 
    context.lineWidth = 10.0;
    context.fillStyle = "#424242"; 
    context.fillRect(0,0,897,532);
    contexto.fillStyle = "#424242"; 
    contexto.fillRect(0,0,897,532);
    context.lineCap = 'round';
    context.lineJoin = 'round';
    contexto.lineCap = 'round';
    contexto.lineJoin = 'round';
    cPush();
    if (tools[tool_default]) {
    tool = new tools[tool_default](); 
    //tool_select.value = tool_default; 
    } 


    function listener(i) {
    document.getElementById(colors[i]).addEventListener('click', function() {
        context.strokeStyle = colors[i];
        laststrokeStyle = context.strokeStyle;
        if(colors[i]!='rainbow'){
            rainlicked = false;
        }
        else rainlicked = true;
    }, false);
    }

    for(var i = 0; i < colors.length; i++) {
    listener(i);
    }

    function typelistener(i){
    document.getElementById(type[i]).addEventListener('click',function(evt){ 
        tool = new tools[type[i]](); 
    },false);
    }

    for(var i = 0; i < type.length; i++) {
    typelistener(i);
    }
    
    // Event Listeners. 
    var mousein = false;
    canvas.addEventListener('mousedown', ev_canvas, false); 
    canvas.addEventListener('mousemove', ev_canvas, false); 
    canvas.addEventListener('mouseup',   ev_canvas, false);
    canvas.addEventListener('mouseleave',function(ev){
        mousein = false;
    },false);
    canvas.addEventListener('click',function(ev){
        if (ev.layerX || ev.layerX == 0) { // Firefox 
            ev._x = ev.layerX; 
            ev._y = ev.layerY; 
            } else if (ev.offsetX || ev.offsetX == 0) { // Opera 
            ev._x = ev.offsetX; 
            ev._y = ev.offsetY; 
            } 
        mousein = true;
        mouseX = ev._x;
        mouseY = ev._y;
        startingX = mouseX;
    },false);

    document.getElementById('Text').addEventListener('click',function(ev){
        inText = true;
    }, false);
    document.getElementById('F1').addEventListener('click',function(ev){
        fonttype = "Arial";
    }, false);
    document.getElementById('F2').addEventListener('click',function(ev){
        fonttype = "Georgia";
    }, false);

    document.addEventListener('keydown',function(ev){
        var setfont = "";
        setfont = context.lineWidth+"px "+ fonttype;
        context.font = setfont;
        if(rainlicked) context.fillStyle = `hsl(${hue}, 100%, 50%)`;
        else context.fillStyle = laststrokeStyle;
        if(mousein&&inText){
        if(ev.key == 'Enter') {
            mouseY += 24;
            mouseX = startingX;
        }else if(ev.key == 'Backspace'){
            cUndo();
        }else{
            context.fillText(ev.key, mouseX, mouseY);
            mouseX+=context.measureText(ev.key).width;
        }
        if (hue >= 360) {
            hue = 0;
        }else{
            hue = hue+50;
        }
        img_update(); 
        cPush();
    }
    },false);}
    

    function ev_canvas (ev) { 
    
    if (ev.layerX || ev.layerX == 0) { // Firefox 
    ev._x = ev.layerX; 
    ev._y = ev.layerY; 
    } else if (ev.offsetX || ev.offsetX == 0) { // Opera 
    ev._x = ev.offsetX; 
    ev._y = ev.offsetY; 
    } 
    // Get the tool's event handler. 
    var func = tool[ev.type];
    if (func) {
    func(ev); 
    } 
    }
    // Create the temporary canvas on top of the canvas, which is cleared each time the user draws. 
    function img_update () { 
    contexto.drawImage(canvas, 0, 0); 
    context.clearRect(0, 0, canvas.width, canvas.height); 
    } 
    var tools = {}; 
    // Chalk tool. 
    tools.chalk = function () { 
        console.log(inText)
    if(inText) return ;
    var tool = this; 
    this.started = false;
    if(rainlicked) context.strokeStyle = `hsl(${hue}, 100%, 50%)`;
    else context.strokeStyle = laststrokeStyle;
    // Begin drawing with the chalk tool. 
    this.mousedown = function (ev) {
    lastX = ev._x;
    lastY = ev._y;
    tool.started = true; 
    }; 
    this.mousemove = function (ev) { 
    if (tool.started&&!inText) {
    console.log('onx')
    context.beginPath(); 
    context.moveTo(lastX, lastY); 
    context.lineTo(ev._x, ev._y); 
    if(rainlicked) context.strokeStyle = `hsl(${hue}, 100%, 50%)`;
    context.stroke(); 
    lastX = ev._x;
    lastY = ev._y;
    if (hue >= 360) {
        hue = 0;
    }else{
        hue = hue+5;
    }
    } 
    };

    this.mouseup = function (ev) { 
    if (tool.started) {
    if(inText)return;
    tool.mousemove(ev); 
    tool.started = false; 
    //rainlicked = false;
    img_update(); 
    cPush();
    } 
    }; 
    };

    tools.eraser = function () {
    if(inText) return;
    var tool = this; 
    this.started = false; 
    // Begin drawing with the chalk tool. 
    this.mousedown = function (ev) { 
    context.beginPath(); 
    context.moveTo(ev._x, ev._y); 
    tool.started = true; 
    }; 
    this.mousemove = function (ev) { 
    if (tool.started) { 
    if(inText)return;
    context.lineTo(ev._x, ev._y); 
    context.strokeStyle = '#424242';
    context.stroke(); 
    } 
    }; 

    this.mouseup = function (ev) { 
    if (tool.started) { 
    if(inText)return;
    tool.mousemove(ev); 
    tool.started = false; 
    img_update(); 
    cPush();
    } 
    }; 
    };

   // The rectangle tool. 
    tools.rect = function () {
        if(inText)return;
    var tool = this; 
    this.started = false; 
    if(rainlicked) context.strokeStyle = `hsl(${hue}, 100%, 50%)`;
    else context.strokeStyle = laststrokeStyle;
    this.mousedown = function (ev) {
    tool.started = true; 
    tool.x0 = ev._x; 
    tool.y0 = ev._y; 
    }; 
    this.mousemove = function (ev) { 
        if(inText)return;
    if (!tool.started) {
    return; 
    }
    if (hue >= 360) {
        hue = 0;
    }else{
        hue = hue+5;
    }
    if(inText)return;
    // This creates a rectangle on the canvas.
    if(rainlicked) context.strokeStyle = `hsl(${hue}, 100%, 50%)`; 
    var x = Math.min(ev._x,  tool.x0), 
    y = Math.min(ev._y,  tool.y0), 
    w = Math.abs(ev._x - tool.x0), 
    h = Math.abs(ev._y - tool.y0); 
    context.clearRect(0, 0, canvas.width, canvas.height);// Clears the rectangle onload. 
        
    if (!w || !h) { 
        return; 
        } 
        context.strokeRect(x, y, w, h);
        }; 
        // Now when you select the rectangle tool, you can draw rectangles. 
        this.mouseup = function (ev) { 
        if (tool.started) { 
            if(inText)return;
        tool.mousemove(ev); 
        tool.started = false; 
        img_update(); 
        cPush();
    } 
    }; 
    };

   // The line tool. 
    tools.line = function () { 
        if(inText)return;
    var tool = this; 
    this.started = false;
    if(rainlicked) context.strokeStyle = `hsl(${hue}, 100%, 50%)`;
    else context.strokeStyle = laststrokeStyle;
    this.mousedown = function (ev) { 
    tool.started = true; 
    tool.x0 = ev._x; 
    tool.y0 = ev._y; 
    }; 
    this.mousemove = function (ev) { 
        if(inText)return;
    if (!tool.started) { 
    return; 
    }
    if (hue >= 360) {
        hue = 0;
    }else{
        hue = hue+5;
    }
    if(rainlicked) context.strokeStyle = `hsl(${hue}, 100%, 50%)`;
    context.clearRect(0, 0, canvas.width, canvas.height); 
    // Begin the line. 
    context.beginPath(); 
    context.moveTo(tool.x0, tool.y0); 
    context.lineTo(ev._x,   ev._y); 
    context.stroke(); 
    context.closePath(); 
    }; 
    // Now you can draw lines when the line tool is seletcted. 
    this.mouseup = function (ev) { 
    if (tool.started) { 
    tool.mousemove(ev); 
    tool.started = false; 
    img_update(); 
    cPush();
    } 
    }; 
    };
    tools.circle = function () {
        if(inText)return;
        var tool = this; 
        this.started = false; 
        if(rainlicked) context.strokeStyle = `hsl(${hue}, 100%, 50%)`;
        else context.strokeStyle = laststrokeStyle;
        this.mousedown = function (ev) {
        if(inText)return;
        tool.started = true; 
        tool.x0 = ev._x; 
        tool.y0 = ev._y; 
        }; 
        this.mousemove = function (ev) {
        if(inText)return; 
        if (!tool.started) {
        return; 
        }
        if (hue >= 360) {
            hue = 0;
        }else{
            hue = hue+5;
        }
        // This creates a rectangle on the canvas.
        if(rainlicked) context.strokeStyle = `hsl(${hue}, 100%, 50%)`; 
        var x = Math.min(ev._x,  tool.x0), 
        y = Math.min(ev._y,  tool.y0), 
        w = Math.abs(ev._x - tool.x0), 
        h = Math.abs(ev._y - tool.y0); 
        var r = eval('(w*w+h*h)/4');
        r = Math.sqrt(r);
        context.clearRect(0, 0, canvas.width, canvas.height);// Clears the rectangle onload. 
        if (!w || !h ||!r) { 
            return; 
            }
            context.beginPath();
            context.arc((ev._x+tool.x0)/2, (ev._y+tool.y0)/2, r, 0, 2 * Math.PI);
            context.stroke();
            }; 
            // Now when you select the rectangle tool, you can draw rectangles. 
            this.mouseup = function (ev) {
            if (tool.started) { 
            tool.mousemove(ev); 
            tool.started = false; 
            img_update(); 
            cPush();
        } 
        }; 
        };
    
    document.getElementById('reset').addEventListener('click', function() {
        context.clearRect(0, 0, canvas.width, canvas.height);
        contexto.clearRect(0, 0, canvas.width, canvas.height);
        context.strokeStyle = "#FFFFFF";// Default line color. 
        context.lineWidth = 10.0;
        context.fillStyle = "#424242"; 
        context.fillRect(0,0,897,532);
        contexto.fillStyle = "#424242"; 
        contexto.fillRect(0,0,897,532);
        context.lineCap = 'round';
        context.lineJoin = 'round';
        contexto.lineCap = 'round';
        contexto.lineJoin = 'round';
        img_update();
        cPush();
        }, false);
    document.getElementById('undo').addEventListener('click', function(){
        cUndo();
    }, false);
    document.getElementById('redo').addEventListener('click', function(){
        cRedo();
    }, false);
    
    var link = document.getElementById("save");
    link.addEventListener('click', function(ev) {
    link.href = canvaso.toDataURL();
    link.download = "mypainting.png";
    }, false);
    
    previewImage = function(input){
        var reader = new FileReader();
        reader.onload = function(ev){
            document.getElementById("preview").setAttribute("src", ev.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
    setImage = function(){
        var x = document.getElementById("imageX").value;
        var y = document.getElementById("imageY").value;
        var h = document.getElementById("imageH").value;
        var w = document.getElementById("imageW").value;
        var image = document.getElementById("preview");
        context.drawImage(image,x,y,h,w);
        img_update();
        cPush();
    }
    init();
      
}, false); }



