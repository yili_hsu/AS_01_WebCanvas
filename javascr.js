var canvas = document.getElementById('art');
var ctx = canvas.getContext("2d");
// var container = canvaso.parentNode; 
// var canvas = document.createElement('canvas'); 
// canvas.id = "tempCanvas";
// canvas.width  = canvaso.width; 
// canvas.height = canvaso.height;
// container.appendChild(canvas); 
// var ctx = canvas.getContext("2d");

let hue = 0;
var rainlicked = false;
let isDrawing = false;
var ispen = true;
var iscircle = false;
var isrectangle = false;
var isline = false;
let lastX=0, lastY=0;
var lastw, lasth;
var laststartx, laststarty;
var nowTool = "";

var offset = canvas.getBoundingClientRect();
var offsetx = offset.left;
var offsety = offset.top;
console.log(offsetx);

canvas.width = innerWidth;
canvas.height = innerHeight;

function drawrectangle(evt){
    
    ctx.lineCap = 'round';
    ctx.lineJoin = 'round';
    //this.mousemove = function (evt) { 
    if(!isDrawing) return;
    var x;
    x = Math.min(evt.x,lastX), 
    y = Math.min(evt.y,lastY), 
    w = Math.abs(evt.x - lastX), 
    h = Math.abs(evt.y - lastY);
    ctx.clearRect(0, 0,canvas.width, canvas.height);
    if (!w || !h) { 
      return; 
      }
      console.log(x, y, w, h);
      ctx.strokeRect(x, y, w, h); 
      this.mouseup = function (evt) { 
      if (isDrawing) { 
      drawrectangle(evt);
      isDrawing = false; 
      //img_update(); 
    }
    };
  // }; 
}

function img_update () { 
  cxto.drawImage(canvas, 0, 0); 
  ctx.clearRect(0, 0, canvas.width, canvas.height); 
  }

function draw(evt) {
    ctx.lineCap = 'round';
    ctx.lineJoin = 'round';
    if(!isDrawing) return;
    if(rainlicked) ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`;
    ctx.beginPath();
    ctx.moveTo(lastX-offsetx, lastY-offsety);
    ctx.lineTo(evt.x-offsetx, evt.y-offsety);
    ctx.stroke();
    console.log("draw")
    lastX = evt.x;
    lastY = evt.y;

    if (hue >= 360) {
        hue = 0;
    }else{
        hue++;
    }
   
}

canvas.addEventListener('mousedown', function(evt) {
  console.log('sss')
  isDrawing = true;
  lastX = evt.x;
  lastY = evt.y;
});

canvas.addEventListener('mousemove', function(evt){
  console.log('move')
  switch(nowTool){
    case 'pen':
    draw(evt);
    break;
    case 'circle':
    break;
    case 'rectangle':
    drawrectangle(evt);
    break;
    default:
    draw(evt);
  }
});

canvas.addEventListener('mouseup', function() {
  isDrawing = false;
});

canvas.addEventListener('mouseleave', function(){
    isDrawing = false;
    rainlicked = false;
});

document.getElementById('reset').addEventListener('click', function() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
}, false);

var colors = ['red', 'blue', 'green', 'purple', 'yellow', 'orange', 'pink', 'black', 'white', 'ebebeb', 'rainbow'];
var size = [1, 3, 5, 10, 15, 20];
var sizeNames = ['default', 'three', 'five', 'ten', 'fifteen', 'twenty'];
var type = ['pen','circle','rectangle','line']

function typelistener(i){
  document.getElementById(type[i]).addEventListener('click',function(){
    nowTool  = type[i];
  },false);
}

function listener(i) {
  document.getElementById(colors[i]).addEventListener('click', function() {
    console.log('aww')
    if(colors[i]!='rainbow')ctx.strokeStyle = colors[i];
    else rainlicked = true;
  }, false);
}

function fontSizes(i) {
  document.getElementById(sizeNames[i]).addEventListener('click', function() {
    ctx.lineWidth = size[i];
    console.log('size[i]')
  }, false);
}

for(var i = 0; i < type.length; i++) {
  typelistener(i);
}

for(var i = 0; i < colors.length; i++) {
  listener(i);
}

for(var i = 0; i < size.length; i++) {
  fontSizes(i);
}


